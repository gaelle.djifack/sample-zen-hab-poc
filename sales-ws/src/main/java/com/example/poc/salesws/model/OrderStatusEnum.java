package com.example.poc.salesws.model;

public enum OrderStatusEnum {
    DRAFT, CONFIRMED, DELIVERED
}
