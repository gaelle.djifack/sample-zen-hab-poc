package com.example.poc.salesws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesWsApplication.class, args);
	}

}
