package com.example.poc.salesws.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Entity
@Table(name = "orders")
@Getter
@Setter
@ToString
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private Long productId;

    @NotNull
    @Column(nullable = false)
    private String customerId;

    @NotNull
    @Column(nullable = false)
    private Long quantity;

    @NotNull
    @Column(nullable = false)
    private OffsetDateTime createdAt;

    @Column
    private OffsetDateTime updatedAt;

    @Column
    private String updatedBy;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatusEnum status;

    @NotNull
    @Size(max = 6)
    @Column(nullable = false, length = 6)
    private String agencyName;

    @PrePersist
    public void beforeCreate(){
        this.createdAt = OffsetDateTime.now();
        System.out.println("in pre persist");
    }

    @PreUpdate
    public void beforeUpdate(){
        if (this.updatedBy == null){
            throw new RuntimeException("UpdatedBy cannot be empty on update operation");
        }
        this.updatedAt = OffsetDateTime.now();
    }
}
