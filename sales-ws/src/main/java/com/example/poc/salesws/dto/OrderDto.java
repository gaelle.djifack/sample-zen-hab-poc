package com.example.poc.salesws.dto;

import com.example.poc.salesws.model.OrderStatusEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

@Getter
@Setter
@ToString
public class OrderDto {

    private Long id;

    @NotNull
    private Long productId;

    @NotNull
    private String customerId;

    @NotNull
    private Long quantity;

    private OffsetDateTime createdAt;

    private OrderStatusEnum status;

    @NotNull
    @Size(max = 6)
    private String agencyName;

    private OffsetDateTime updatedAt;

    private String updatedBy;
}
