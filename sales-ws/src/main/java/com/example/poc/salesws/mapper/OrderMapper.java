package com.example.poc.salesws.mapper;

import com.example.poc.salesws.dto.OrderDto;
import com.example.poc.salesws.model.Order;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

	Order toEntity(OrderDto dto);
	OrderDto toDto(Order order);
	List<OrderDto> toDto(List<Order> orders);
}
