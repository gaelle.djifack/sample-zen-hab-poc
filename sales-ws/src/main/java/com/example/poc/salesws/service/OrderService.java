package com.example.poc.salesws.service;

import com.example.poc.salesws.dto.OrderDto;
import com.example.poc.salesws.mapper.OrderMapper;
import com.example.poc.salesws.repository.OrderRepository;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.IDToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderService {

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper){
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    public OrderDto createOrder(final OrderDto orderDto) {
        var order = this.orderMapper.toEntity(orderDto);
        order.setId(null);
        order.setCustomerId(this.getUsername());
        var created = this.orderRepository.save(order);
        return this.orderMapper.toDto(created);
    }

    public List<OrderDto> findOrders() {
        var agencies = getUserAgencies();
        var result = CollectionUtils.isEmpty(agencies) ? this.orderRepository.findAll() :
                this.orderRepository.findByAgencyNameIn(agencies);

        if (!CollectionUtils.isEmpty(result)){
            return this.orderMapper.toDto(result) ;
        }
        return Collections.emptyList();
    }

    public List<OrderDto> findOrdersByCustomer() {
        var result = this.orderRepository.findByCustomerId(this.getUsername());
        return this.orderMapper.toDto(result);
    }

    private String getUsername() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return securityContext.getAuthentication().getName();
    }

    private List<String> getUserAgencies(){
        KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken)
                SecurityContextHolder.getContext().getAuthentication();

        Principal principal = (Principal) authentication.getPrincipal();
        System.out.println("User Name : " + principal.getName());

        if (principal instanceof KeycloakPrincipal) {
            KeycloakPrincipal kPrincipal = (KeycloakPrincipal) principal;
            IDToken token = kPrincipal.getKeycloakSecurityContext().getToken();

            Map<String, Object> customClaims = token.getOtherClaims();
            var agencies = customClaims.get("agencies");
            System.out.println("User agency : " + agencies);
            if (agencies != null && StringUtils.hasText(agencies.toString())) {
                return Arrays.asList(agencies.toString().split(";"));
            }
        }
        return Collections.emptyList();
    }
}
