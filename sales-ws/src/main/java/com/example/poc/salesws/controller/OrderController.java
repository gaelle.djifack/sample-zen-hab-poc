package com.example.poc.salesws.controller;

import com.example.poc.salesws.dto.OrderDto;
import com.example.poc.salesws.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/sales")
public class OrderController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PreAuthorize("hasRole('ROLE_customer')")
    @PostMapping
    public OrderDto createOrder(@RequestBody @Valid final OrderDto orderDto){
        logger.info("Creating order : {}", orderDto);
        var created = this.orderService.createOrder(orderDto);
        logger.info("Product created : {}", created.getId());
        return created;
    }

    @PreAuthorize("hasRole('ROLE_manager') or hasRole('ROLE_seller')")
    @GetMapping
    public List<OrderDto> getOrders(){
        var listOrders = this.orderService.findOrders();
        logger.info("Product created : {}", listOrders.size());
        return listOrders;
    }

    @PreAuthorize("hasRole('ROLE_customer')")
    @GetMapping("/me")
    public List<OrderDto> getOrdersByCustomer(){
        var listOrders = this.orderService.findOrdersByCustomer();
        logger.info("Product created : {}", CollectionUtils.isEmpty(listOrders) ? 0 : listOrders.size());
        return listOrders;
    }
}
