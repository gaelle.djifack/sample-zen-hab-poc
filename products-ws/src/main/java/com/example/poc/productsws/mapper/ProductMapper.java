package com.example.poc.productsws.mapper;

import com.example.poc.productsws.dto.ProductDto;
import com.example.poc.productsws.dto.ProductStockDto;
import com.example.poc.productsws.model.Product;
import com.example.poc.productsws.model.ProductStock;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product toEntity(ProductDto dto);
    ProductDto toDto(Product entity);
    List<ProductDto> toDto(List<Product> entities);

    List<ProductStockDto> map(List<ProductStock> entities);
}
