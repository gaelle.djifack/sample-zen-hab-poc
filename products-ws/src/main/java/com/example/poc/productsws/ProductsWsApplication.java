package com.example.poc.productsws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsWsApplication.class, args);
	}

}
