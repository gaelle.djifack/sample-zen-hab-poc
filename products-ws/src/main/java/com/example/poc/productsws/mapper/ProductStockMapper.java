package com.example.poc.productsws.mapper;

import com.example.poc.productsws.dto.ProductStockDto;
import com.example.poc.productsws.model.ProductStock;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductStockMapper {

    ProductStock toEntity(ProductStockDto dto);
    ProductStockDto toDto(ProductStock entity);
}
