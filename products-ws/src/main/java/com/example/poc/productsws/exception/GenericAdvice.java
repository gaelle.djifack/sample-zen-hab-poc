package com.example.poc.productsws.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GenericAdvice {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Order(100)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    ResponseEntity<ApiError> globalExceptionHandler(Throwable ex) {
        return getResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }

    @ResponseBody
    @Order(0)
    @ExceptionHandler(AccessDeniedException.class)
    ResponseEntity<ApiError> accessDeniedHandler(AccessDeniedException ex) {
        return getResponse(HttpStatus.FORBIDDEN, ex);
    }

    @ResponseBody
    @Order(1)
    @ExceptionHandler(EntityNotFoundException.class)
    ResponseEntity<ApiError> entityNotFoundHandler(EntityNotFoundException ex) {
        return getResponse(HttpStatus.NOT_FOUND, ex);
    }

    @ResponseBody
    @Order(2)
    @ExceptionHandler(ForbiddenAccessException.class)
    ResponseEntity<ApiError> forbiddenAccessHandler(ForbiddenAccessException ex) {
        return getResponse(HttpStatus.FORBIDDEN, ex);
    }

    private ResponseEntity<ApiError> getResponse(HttpStatus status, Throwable ex){
        logger.error("Catched exception - {}", status, ex);
        ApiError error = new ApiError(status, ex);
        return new ResponseEntity<>(error, error.getStatus());
    }
}
