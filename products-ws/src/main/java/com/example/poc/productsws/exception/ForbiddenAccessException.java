package com.example.poc.productsws.exception;

public class ForbiddenAccessException extends RuntimeException {

    private static final long serialVersionUID = -3773906509248972520L;

    private ForbiddenAccessException(String message) {
        super(message);
    }

    public static ForbiddenAccessException getException(String entity, Object value) {
        String message = "Operation not allowed on '" + entity + "' with value " + value;
        return new ForbiddenAccessException(message);
    }
}
