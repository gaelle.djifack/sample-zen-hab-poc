package com.example.poc.productsws.repository;

import com.example.poc.productsws.model.ProductStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductStockRepository extends JpaRepository<ProductStock, Long> {

    List<ProductStock> findByProductId(Long productId);

    Optional<ProductStock> findByProductIdAndAgency(Long productId, String agencyName);
}
