package com.example.poc.productsws.service;

import com.example.poc.productsws.dto.ProductDto;
import com.example.poc.productsws.dto.ProductStockDto;
import com.example.poc.productsws.exception.EntityNotFoundException;
import com.example.poc.productsws.exception.ForbiddenAccessException;
import com.example.poc.productsws.mapper.ProductMapper;
import com.example.poc.productsws.mapper.ProductStockMapper;
import com.example.poc.productsws.model.ProductStock;
import com.example.poc.productsws.repository.ProductRepository;
import com.example.poc.productsws.repository.ProductStockRepository;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.IDToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;

    private final ProductStockRepository productStockRepository;

    private final ProductMapper productMapper;

    private final ProductStockMapper productStockMapper;

    public ProductService(ProductRepository productRepository, ProductStockRepository productStockRepository,
                          ProductMapper productMapper, ProductStockMapper productStockMapper) {
        this.productRepository = productRepository;
        this.productStockRepository = productStockRepository;
        this.productMapper = productMapper;
        this.productStockMapper = productStockMapper;
    }

    public List<ProductDto> getAllProducts() {
        var products = this.productRepository.findAll();
        return this.productMapper.toDto(products);
    }

    public ProductDto createProduct(final ProductDto productDto) {
        var product = this.productMapper.toEntity(productDto);
        product.setId(null);
        product.setCreatedBy(this.getUsername());
        var created = this.productRepository.save(product);
        return this.productMapper.toDto(created);
    }

    private String getUsername() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return securityContext.getAuthentication().getName();
    }

    public ProductStockDto updateProductStock(long id, ProductStockDto productStockDto) {
        var productOptional = this.productRepository.findById(id);
        if (productOptional.isEmpty()){
            throw EntityNotFoundException.getException("product", id);
        }
        if (!this.getUserAgencies().contains(productStockDto.getAgency())){
            throw ForbiddenAccessException.getException("agency", productStockDto.getAgency());
        }
        var stockOptional = this.productStockRepository
                .findByProductIdAndAgency(id, productStockDto.getAgency());
        ProductStock stock = stockOptional.orElseGet(() -> {
            var newStock = new ProductStock();
            newStock.setAgency(productStockDto.getAgency());
            newStock.setProduct(productOptional.get());
            return newStock;
        });
        stock.setQuantity(productStockDto.getQuantity());
        var updatedStock = this.productStockRepository.save(stock);
        return this.productStockMapper.toDto(updatedStock);
    }

    private List<String> getUserAgencies(){
        KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken)
                SecurityContextHolder.getContext().getAuthentication();

        Principal principal = (Principal) authentication.getPrincipal();
        System.out.println("User Name : " + principal.getName());

        if (principal instanceof KeycloakPrincipal) {
            KeycloakPrincipal kPrincipal = (KeycloakPrincipal) principal;
            IDToken token = kPrincipal.getKeycloakSecurityContext().getToken();

            Map<String, Object> customClaims = token.getOtherClaims();
            var agencies = customClaims.get("agencies");
            System.out.println("User agency : " + agencies);
            if (agencies != null && StringUtils.hasText(agencies.toString())) {
                return Arrays.asList(agencies.toString().split(";"));
            }
        }
        return Collections.emptyList();
    }
}
