package com.example.poc.productsws.dto;

import com.example.poc.productsws.model.ProductStock;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
public class ProductDto {

    private Long id;

    @NotNull
    @Size(max = 200)
    private String name;

    @Size(max = 250)
    private String description;

    private OffsetDateTime createdAt;

    private String createdBy;

    private String updatedBy;

    private OffsetDateTime updatedAt;

    private List<ProductStock> stocks;
}
