/**
 * 
 */
package com.example.poc.productsws.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Gaelle Djifack
 *
 */
@Getter
@ToString
public class ApiError {

	private final HttpStatus status;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private final LocalDateTime timestamp;
	
	private final String message;
	
	private String debugMessage;
	
	public ApiError(HttpStatus status, Throwable ex) {
		this.status = status;
		this.message = ex.getMessage();
		this.timestamp = LocalDateTime.now();
		if(Objects.nonNull(ex.getSuppressed())) {
			this.debugMessage = Arrays.stream(ex.getSuppressed())
				.map(Throwable::getMessage)
				.collect(Collectors.joining(";"));
		}
	}
}
