package com.example.poc.productsws.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductStockDto {

    private Long id;

    @NotNull
    private String agency;

    @NotNull
    private Long quantity;
}
