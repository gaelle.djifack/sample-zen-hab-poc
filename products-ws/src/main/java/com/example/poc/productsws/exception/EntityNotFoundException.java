package com.example.poc.productsws.exception;

public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3773906509248972520L;

	private EntityNotFoundException(String message) {
		super(message);
	}
	
	public static EntityNotFoundException getException(String entity, Object id) {
		String message = "Resource " + entity + " with Id " + id + " not found";
		return new EntityNotFoundException(message);
	}
}
