package com.example.poc.productsws.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Table(name = "products")
@Getter
@Setter
@ToString
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name", nullable = false, length = 200)
    private String name;

    @Size(max = 250)
    @Column(name = "description", length = 250)
    private String description;

    @NotNull
    @Column(nullable = false)
    private OffsetDateTime createdAt;

    @NotNull
    @Column
    private String createdBy;

    @Column
    private String updatedBy;

    @Column
    private OffsetDateTime updatedAt;

    @OneToMany(targetEntity = ProductStock.class, mappedBy = "product")
    private List<ProductStock> stocks;

    @PrePersist
    public void beforeCreate(){
        this.createdAt = OffsetDateTime.now();
    }

    @PreUpdate
    public void beforeUpdate(){
        if (this.updatedBy == null){
            throw new RuntimeException("UpdatedBy cannot be empty on update operation");
        }
        this.updatedAt = OffsetDateTime.now();
    }
}
