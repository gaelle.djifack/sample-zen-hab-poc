/**
 * 
 */
package com.example.poc.productsws.security;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * Configuration class for OpenAPI.
 * 
 * @author Gaelle Djifack
 *
 */
@Configuration
public class SpringdocConfig {

	/**
	 * Method to customize OPEN API specifications.
	 * 
	 * @return the new description of our open api
	 */
	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI()
				.components(new Components().addSecuritySchemes("bearer-jwt",
						new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")
								.in(SecurityScheme.In.HEADER).name("Authorization")))
				.info(getInfo())
				.addSecurityItem(new SecurityRequirement().addList("bearer-jwt", Arrays.asList("read", "write")));
	}
	
	/**
	 * Create info object to describe our API.
	 * 
	 * @return the info object
	 */
	private Info getInfo() {
		return new Info().title("Product WS POC API")
				.version("0.1")
				.description("This api is a proof of concept. "
						+ "This API use spring security with keycloak.");
	}
}
