package com.example.poc.productsws.controller;

import com.example.poc.productsws.dto.ProductDto;
import com.example.poc.productsws.dto.ProductStockDto;
import com.example.poc.productsws.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<ProductDto> getAllProducts(){
        logger.info("Get all products");
        var products = this.productService.getAllProducts();
        logger.info("Products founded : {}", products.size());
        return products;
    }

    @PreAuthorize("hasRole('ROLE_manager')")
    @PostMapping
    public ProductDto createProduct(@RequestBody @Valid ProductDto productDto){
        logger.info("Create product - {}", productDto);
        var product = this.productService.createProduct(productDto);
        logger.info("Product created : {}", product.getId());
        return product;
    }

    @PreAuthorize("hasRole('ROLE_seller')")
    @PostMapping("/{id}/stocks")
    public ProductStockDto updateStock(@PathVariable(name = "id") long id,
                                  @RequestBody @Valid ProductStockDto productStockDto){
        logger.info("Update product stock {} - {}", id, productStockDto);
        var result = this.productService.updateProductStock(id, productStockDto);
        logger.info("Product stock updated {} - {}", id, result);
        return result;
    }
}
