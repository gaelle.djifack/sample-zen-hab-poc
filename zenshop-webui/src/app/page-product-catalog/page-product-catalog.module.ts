import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCatalogComponent } from './product-catalog/product-catalog.component';



@NgModule({
  declarations: [
    ProductCatalogComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProductCatalogComponent
  ]
})
export class PageProductCatalogModule { }
