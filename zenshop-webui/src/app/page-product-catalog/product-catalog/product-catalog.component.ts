import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/core/models';
import { ProductsService } from 'src/app/core/products.service';

@Component({
  selector: 'app-product-catalog',
  templateUrl: './product-catalog.component.html',
  styleUrls: ['./product-catalog.component.css']
})
export class ProductCatalogComponent implements OnInit {

  products : Product[] = [];

  constructor(private productService: ProductsService) { }

  ngOnInit(): void {
    this.productService.getAll().subscribe(res => this.products = res);
  }

}
