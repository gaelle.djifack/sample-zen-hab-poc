import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product, Stock } from './models';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  backendUrl = "http://localhost:8084/products"

  constructor(private http: HttpClient) { }

  getAll(): Observable<Product[]> {  
    const url = new URL(`${this.backendUrl}`);
    return this.http.get<Product[]>(url.href);
  }

  createProduct(product: Product): Observable<Product> {  
    const url = new URL(`${this.backendUrl}`);
    return this.http.post<Product>(url.href, product);
  }

  updateProductStock(id: number, stock: Stock): Observable<Product> {  
    const url = new URL(`${this.backendUrl}/${id}/stocks`);
    return this.http.post<Product>(url.href, stock);
  }
}
