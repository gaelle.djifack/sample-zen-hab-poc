import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from './models';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  backendUrl = "http://localhost:8082/sales"

  constructor(private http: HttpClient) { }

  getAllOrders(): Observable<Order[]> {  
    const url = new URL(`${this.backendUrl}`);
    return this.http.get<Order[]>(url.href);
  }

  getUserOrders(): Observable<Order[]> {  
    const url = new URL(`${this.backendUrl}/me`);
    return this.http.get<Order[]>(url.href);
  }

  createOrder(order: Order): Observable<Order> {  
    const url = new URL(`${this.backendUrl}`);
    return this.http.post<Order>(url.href, order);
  }

}
