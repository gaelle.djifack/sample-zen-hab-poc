export class UserDetails{
    profile: string;
    agencies: string;
}

export class Order {
    id: number;
    agencyName: string;
    customerId: string;
    status: string;
    quantity: number;
    productId: number;
    createdAt: Date;
    updatedAt: Date;
    updatedBy: string;
}

export class Product {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    stocks: Stock[];
}

export class Stock {
    id: number;
    agency: string;
    quantity: number;
}