import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/core/models';
import { SalesService } from 'src/app/core/sales.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {

  orders : Order[] = [];

  constructor(private salesService: SalesService) { }

  ngOnInit(): void {
    this.salesService.getUserOrders().subscribe(res => this.orders = res);
  }

}
