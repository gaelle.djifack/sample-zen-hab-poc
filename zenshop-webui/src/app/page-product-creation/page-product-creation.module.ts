import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCreationComponent } from './product-creation/product-creation.component';



@NgModule({
  declarations: [
    ProductCreationComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProductCreationComponent
  ]
})
export class PageProductCreationModule { }
