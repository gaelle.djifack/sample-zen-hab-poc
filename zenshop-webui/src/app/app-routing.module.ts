import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { ForbiddenComponent } from './page-forbidden/forbidden/forbidden.component';
import { MyOrdersComponent } from './page-my-orders/my-orders/my-orders.component';
import { OrderCreationComponent } from './page-order-creation/order-creation/order-creation.component';
import { OrdersListComponent } from './page-orders-list/orders-list/orders-list.component';
import { ProductCatalogComponent } from './page-product-catalog/product-catalog/product-catalog.component';
import { ProductCreationComponent } from './page-product-creation/product-creation/product-creation.component';
import { ProductStockEditionComponent } from './page-product-stock-edition/product-stock-edition/product-stock-edition.component';

const routes: Routes = [
  {
    path: 'product-catalog', component: ProductCatalogComponent
  },
  {
    path: 'new-product', 
    component: ProductCreationComponent,
    canActivate: [AuthGuard],
    data: {roles: ['manager']}
  },
  {
    path: 'product-stock-edition', 
    component: ProductStockEditionComponent,
    canActivate: [AuthGuard],
    data: {roles: ['seller']}
  },
  {
    path: 'orders', 
    component: OrdersListComponent,
    canActivate: [AuthGuard],
    data: {roles: ['manager', 'seller']}
  },
  {
    path: 'new-order', component: OrderCreationComponent,
    canActivate: [AuthGuard],
    data: {roles: ['customer']}
  },
  {
    path: 'my-orders', 
    component: MyOrdersComponent,
    canActivate: [AuthGuard],
    data: {roles: ['customer']}
  },
  {path: 'forbidden', component: ForbiddenComponent},
  {path: '', redirectTo: '/product-catalog', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
