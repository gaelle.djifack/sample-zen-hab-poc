import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from 'src/app/core/models';
import { SalesService } from 'src/app/core/sales.service';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {

  orders : Order[] = [];

  constructor(private salesService: SalesService) { }

  ngOnInit(): void {
    this.salesService.getAllOrders().subscribe(res => this.orders = res);
  }

}
