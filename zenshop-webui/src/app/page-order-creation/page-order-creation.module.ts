import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderCreationComponent } from './order-creation/order-creation.component';



@NgModule({
  declarations: [
    OrderCreationComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    OrderCreationComponent
  ]
})
export class PageOrderCreationModule { }
