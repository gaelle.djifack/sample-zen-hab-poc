import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductStockEditionComponent } from './product-stock-edition/product-stock-edition.component';



@NgModule({
  declarations: [
    ProductStockEditionComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProductStockEditionComponent
  ]
})
export class PageProductStockEditionModule { }
