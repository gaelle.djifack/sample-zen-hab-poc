import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductStockEditionComponent } from './product-stock-edition.component';

describe('ProductStockEditionComponent', () => {
  let component: ProductStockEditionComponent;
  let fixture: ComponentFixture<ProductStockEditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductStockEditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductStockEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
