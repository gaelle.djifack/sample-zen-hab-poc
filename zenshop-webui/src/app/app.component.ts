import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { UserDetails } from './core/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'zenshop-webui';
  userDetails: UserDetails;

  constructor(private keycloakService: KeycloakService,
    private readonly router: Router){
  }

  ngOnInit(): void {
    this.loadUserDetails();
  }

  isAuthenticated() : boolean{
    return this.keycloakService.getKeycloakInstance().authenticated;
  }

  login() {
    this.keycloakService.login();
  }

  logout() {
    this.keycloakService.logout(window.location.origin);
  }

  loadUserDetails(){
    const tokenParsed = this.keycloakService.getKeycloakInstance().tokenParsed;
    console.log(this.keycloakService.getKeycloakInstance().token);

    this.userDetails = {
      profile : tokenParsed?.preferred_username,
      agencies: tokenParsed?.agencies ? tokenParsed.agencies.split(';') : []
    };
    /*this.keycloakService.loadUserProfile().then(profile => {
      const basicProfile = profile as any;
      this.userDetails = {
        profile : profile.username,
        agencies: basicProfile.attributes.agencies ? basicProfile.attributes.agencies[0].split(';') : []
      };
      console.log(this.userDetails);
    }).catch(err => console.warn(err));*/
  }
}
